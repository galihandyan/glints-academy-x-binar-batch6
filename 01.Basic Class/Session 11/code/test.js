/*
 1. Create a class called Human
 2. Create sub class called Chef

 Human can Cook();
 Human can Introduce();

 Chef cuisines;
 Chef Type => Italian, French
 Chef can cook() but better (use override method)
 Chef will introduce themself as ${type} chef
 */
Array.prototype.sample = function () {
    return this[
        Math.floor(
            Math.random() * this.length
        )
    ]
}

class Human {
    constructor(props) {
        let { name, address, lang } = props
        this.name = name;
        this.address = address;
        this.lang = lang;
    }

    introduce() {
        console.log(`Hello, My name is ${this.name}`);
    }

    chefOrNot() {
        if (this.constructor.isCanCookBetter == true) {
            console.log(`${this.name} is a Master of ${this.type} Chef`);
        } else {
            console.log(`${this.name} is not a Chef`);
        }
    }

    static isCanCookBetter = false;
}

class Chef extends Human {
    constructor(props) {
        super(props);
        this.cuisines = props.cuisines;
        this.type = props.type
    }

    static isCanCookBetter = true

    cookingSkill() {
        console.log(`The one of my cuisines is ${this.cuisines.sample()}`);
    }

}

const Galih = new Chef({
    name: "Galih",
    address: "Yogyakarta",
    lang: "Indonesia",
    type: "Javanese",
    cuisines: ["Bakmi", "Gudeg", "Sayur Lodeh", "lotek"]
})

Human.prototype.welcome = function (name) {
    Galih.introduce();
    Galih.cookingSkill();
    Galih.chefOrNot();
}


Galih.welcome();

