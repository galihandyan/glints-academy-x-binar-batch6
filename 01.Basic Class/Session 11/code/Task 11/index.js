
/*
Create class Human
 - name
 - address
 - language
Create child class Human. Male and Female
 - gender
can learn a new language
marry person must be both understand the same language
*/

class Human {
    constructor (props){
        let { name, languages, gender } = props;
        this.name = name;
        this.languages = [languages];
        this.gender = gender;
    }

    learnNewLanguage (language){

        //includes function to check same value on the array

        if (!this.languages.includes(language)) {
            this.languages.push(language)
        }
    }

    marryHuman(anotherHuman){
        // make sure both will be married understand the same language
        let marry = false;

        this.languages.forEach(i => {
            if (!this.languages.includes(anotherHuman.languages)) {
                console.log("belajar lagi");
            }
        });
    }

    createHuman(){
        // create human with readline then keep it to .json file
    }

}

const human1 = new Human({
    name:"Budi",
    languages:"Javanese",
    gender:"Male"
});

const human2 = new Human({
    name:"Ani",
    languages:"Indonesia",
    gender:"Female"
});

console.log(human1);
console.log(human2);

human1.marryHuman(human2);