
const circle = require('../calculate/circleArea.js');
const cube = require('../calculate/cubeVolume.js');
const square = require('../calculate/squareArea.js');
const triangle = require('../calculate/triangleArea.js');
const tube = require('../calculate/tubeVolume.js');
// const info = require('../controller/info.js');


module.exports = {
    circleArea(r) {
        let phi = 3.14;
        let circleArea = phi * (r * r);
        return {
            status:true,
            result:circleArea
        };
    },

    cubeVolume() {

    },

    squareArea() {

    },

    triangleArea() {

    },

    tubeVolume() {

    },

    info(req, res) {
        res.send("This is info");
    }
}