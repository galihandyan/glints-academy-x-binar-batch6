const express = require('express');
const router = express.Router();
const controller = require('./controller/controller.js');

// router get
router.get('/info', controller.info);

// router post
router.post('/calculate/circleArea', (req, res) => {
    return res.send(controller.circleArea(req.body.r));
});
router.post('/calculate/cubeVolume', (req, res) => {
    return res.send(controller.cubeVolume(req.body.side));
});
router.post('/calculate/squareArea', controller.squareArea);
router.post('/calculate/triangleArea', controller.triangleArea);
router.post('/calculate/tubeVolue', controller.tubeVolume);


module.exports = router;