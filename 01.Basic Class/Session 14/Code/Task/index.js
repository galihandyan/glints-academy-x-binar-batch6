const express = require ('express');
const app = express();
const router = require('./route.js');

app.use(express.json());
app.use(cors());

app.get('/', function (req, res) {
    res.send("Welcome to Daily Task Session 14 - Day Class! \n Dont forget to look at /info");
})

app.use('/', router);

app.listen(3000, () => {
    console.log("Listening on port 3000!");
})

