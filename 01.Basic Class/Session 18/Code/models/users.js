'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
        isLowercase: true
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        min: 6
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        instance.password = bcrypt.hashSync(instance.password, 10);
      }
    }
  });
  Users.associate = function (models) {
    // associations can be defined here
  };
  return Users;
};