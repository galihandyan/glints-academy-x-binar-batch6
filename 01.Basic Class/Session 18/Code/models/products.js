'use strict';
module.exports = (sequelize, DataTypes) => {
  const Products = sequelize.define('Products', {
    name: {
      type : DataTypes.STRING
    },
    price: {
      type : DataTypes.INTEGER
    },
    stock: {
      type : DataTypes.INTEGER
    }
  }, {});
  Products.associate = function(models) {
    // associations can be defined here
  };
  return Products;
};