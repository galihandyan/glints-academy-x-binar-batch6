const { Users, Products } = require('../models')
const bcrypt = require('bcryptjs')
const handler = require('../helper/statusHandler')
const jwt = require('jsonwebtoken')
require('dotenv').config()
const key = process.env.SECRET_KEY;

class User {
    static async register(req, res) {
        try {
            let msg = "Data has been created"
            let data = await Users.create(req.body);

            handler.success(res, 201, msg, data)

        } catch (err) {
            handler.fail(res, 422, err);
        }
    }

    static async login(req, res) {
        try {
            // find email to DB
            let data = await Users.findOne({ where: { email: req.body.email.toLowerCase() } });
            if (!data) throw "Email doesn't exist";

            // compare bcrypt
            let check = bcrypt.compareSync(req.body.password, data.password);
            if (!check) throw "Password incorrect";

            // create token
            let token = jwt.sign({
                id: data.id,
                email: data.email
            }, key)

            // define msg and do handler
            let msg = "Login Success";
            handler.success(res, 201, msg, token)

        } catch (err) {
            handler.fail(res, 400, err)
        }
    }

    static me(req, res) {
        handler.success(res, 200, req.user)
    }
}

class Product {
    static showAll(req, res) {
        Products.findAll()
            .then((data) => handler.success(res, 200, data))
    }

    static async showOne(req, res, next) {
        try {
            let data = await Products.findByPk(req.params.productId);
            if (data == null) throw "Data not found"

            let msg = "Data found";
            handler.success(res, 200, msg, data);
        } catch (err) {
            handler.fail(res, 400, err)
        }
    }

    static add(req, res) {
        Products.create(req.body)
            .then((data) => {
                let msg = "New data created"
                handler.success(res, 200, msg, data)
            })
            .catch((err) => handler.fail(res, 422, err.message))
    }

    static async update(req, res, next) {
        try {
            let idProduct = req.params.productId;
            let msg = "Product with ID " + idProduct + " has been updated";

            let data = await Products.update(req.body, { where: { id: idProduct } })
            if (data == 0) msg = "Nothing updated";

            let newData = await Products.findByPk(idProduct);
            handler.success(res, 201, msg, newData)
        }
        catch (err) {
            handler.fail(res, 422, err.message)
        }
    }

    static async del(req, res) {
        try {
            let idProduct = req.params.productId
            let oldData = await Products.findByPk(idProduct)
            
            let msg = "Data with ID " + idProduct + " has been deleted"
            let data = await Products.destroy({ where: { id:idProduct } })
            if (data == 0) throw "Data to delete does not exist"
            
            handler.success(res, 201, msg, oldData)
        } catch (err) {
            handler.fail(res, 400, err)
        }
    }

}

module.exports = {
    User,
    Product
}