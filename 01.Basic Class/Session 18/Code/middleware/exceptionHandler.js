module.exports = [
    // 404 Not Found Handler
    function(req, res, next) {
      res.status(404);
      next(new Error("Page Not found"))
    },
  
    // Error Formatter
    function(err, req, res, next) {
      if (res.statusCode == 200) res.status(500);
  
      res.json({
        status: 'fail',
        errors: [err.message]
      })
    },
  ]