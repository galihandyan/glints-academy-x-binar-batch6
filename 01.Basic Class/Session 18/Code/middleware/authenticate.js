const jwt = require('jsonwebtoken');
const {Users} = require('../models');
require ('dotenv').config();
const key = process.env.SECRET_KEY;

module.exports = async (req, res, next) => {
    try{
        let token = req.headers.auth;
        let payload = await jwt.verify(token, key);

        let user = await Users.findByPk(payload.id);
        req.user = user;
        next();
    }

    catch(err) {
        res.status(401);
        next(new Error("You don't have permission / Invalid Token"));
    }
}