const express = require('express');
const app = express();
const router = require('./router');
const cors = require('cors');
const morgan = require('morgan');
const exception = require('./middleware/exceptionHandler');

app.use(morgan('dev'));
app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.json({
        status:"success",
        message:"Welcome to The Home Page"
    })
})
app.use('/', router);

exception.forEach(handler => {
    app.use(handler);
});

app.listen(3000, () =>{
    console.log(`Server running on PORT 3000`);
})

