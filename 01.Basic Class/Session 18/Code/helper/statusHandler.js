module.exports = {
    success(res, code, msg, data) {
        res.status(code).json({
            status: "Success",
            message: msg,
            data: data
        })
    },

    fail(res, code, err) {
        res.status(code).json({
            status: "fail",
            message: err
        })
    }
}