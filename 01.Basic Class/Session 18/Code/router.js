const express = require('express');
const router = express.Router();
const control = require('./controller/mainController');
const auth = require('./middleware/authenticate');


// router for user
router.post('/user/register', control.User.register);
router.post('/user/login', control.User.login);
router.get('/user/me', auth, control.User.me);

// router for product
router.get('/product', control.Product.showAll);
router.post('/product/add', auth, control.Product.add);
router.get('/product/:productId', control.Product.showOne);
router.put('/product/:productId', auth, control.Product.update);
router.delete('/product/:productId', auth, control.Product.del);

module.exports = router;