const Book = require('./models/book');

const dasKapital = new Book({
  title: "More Das",
  author: "Karl Marx",
  price: 80000,
  publisher: "Unknown"
});

let book2 = Book.find(1);
console.log(book2);
/* Mix-ins => Create helper between class, usually it is being to help the class so it is not bloated */