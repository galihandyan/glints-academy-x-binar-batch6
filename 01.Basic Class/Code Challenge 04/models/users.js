'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
        isLowercase: true
      }
    },
    password: DataTypes.STRING,
    role: {
      type: DataTypes.STRING,
      defaultValue: 'member'
    }
  }, {
    hooks: {
      beforeValidate: instance=>{
        instance.email = instance.email.toLowerCase();
      },
      beforeCreate:instance=>{
        instance.password = bcrypt.hashSync(instance.password, 10);
      }
    }
  });
  Users.associate = function (models) {
    // associations can be defined here
    Users.hasMany(models.Posts,{
      foreignKey:"users_id"
    })
  };
  return Users;
};