'use strict';
module.exports = (sequelize, DataTypes) => {
  const Posts = sequelize.define('Posts', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    approved: {
      type:DataTypes.BOOLEAN,
      defaultValue:false
    },
    users_id: DataTypes.INTEGER
  }, {});
  Posts.associate = function(models) {
    // associations can be defined here
    Posts.belongsTo(models.Users,{
      foreignKey:'users_id'
    })
  };
  return Posts;
};