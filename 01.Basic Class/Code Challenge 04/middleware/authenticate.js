const jwt = require('jsonwebtoken');
const { Users } = require('../models');
require('dotenv').config();

module.exports = async (req, res, next) => {
    try {
        let token = req.headers.auth;
        let payload = await jwt.verify(token, process.env.SECRET_KEY);

        req.user = await Users.findByPk(payload.id);
        next();
    }
    catch(err){
        res.json({
            status:"fail",
            data:"Do not have permission or Token is invalid"
        })
    }
}