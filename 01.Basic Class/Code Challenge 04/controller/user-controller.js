const { Users } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const resp = require('../helper/response');

class User {
    static async register(req, res, next) {
        try {
            let data = await Users.create(req.body);
            let key = "user";

            resp(res, 201, key, data);
        } catch (err) {
            res.status(400).json({
                status: "fail",
                data: err.messsage
            })
        }
    }

    static async login(req, res, next) {
        try {
            let data = await Users.findOne({ where: { email: req.body.email.toLowerCase() } })
            if (!data) throw "Email doesn't exist";

            let check = await bcrypt.compareSync(req.body.password, data.password);
            if (!check) throw "Password incorrect";

            let token = jwt.sign({
                id: data.id,
                nama: data.nama,
                email: data.email,
                role: data.role
            }, process.env.SECRET_KEY);

            let key = "token"
            resp(res, 201, key, token);
        } catch (err) {
            res.status(400).json({
                status: "fail",
                data: err
            })
        }
    }
}

module.exports = User;