const express = require('express');
const app = express();
const morgan = require('morgan');
const router = require('./router/main-router');
require('dotenv').config();

app.use(express.json());
app.use(morgan('tiny'));

app.get('/', (req, res) => {
    res.status(200).json({
        status: "success",
        data: "welcome to Code Challenge 04"
    })
});

app.use('/api', router);

app.listen(3000, ()=>{
    console.log('Server running on port 3000');
})