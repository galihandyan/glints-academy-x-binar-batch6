const User = require('../models').Users;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

function register(req, res) {

    User.create({
        email: req.body.email,
        password: req.body.password
    }).then((data) =>
        res.status(401).json({
            status: "success",
            data
        })
    )
    .catch((err) => {
       res.status(422).json({
           status: "fail",
           message:err
       })
    })
    ;
}

function login(req, res) {

    User.findOne({
        where: {
            email: req.body.email.toLowerCase()
        }
    })
        .then((data) => {
            if (!data) {
                res.status(401).json({
                    status: "fail",
                    message: "incorrect email"
                })
            }

            if (!bcrypt.compareSync(req.body.password, data.password)) {
                res.status(401).json({
                    status:"fail",
                    message:"incorrect password"
                })
            }

            // sukses login
            const token = jwt.sign({
                id:data.id,
                email:data.email
            }, 'secretkey');

            res.status(201).json({
                status: "success",
                message: "login success",
                token: token
            })
        })

        .catch((err) => {
            return res.status(400).json({
                status: "fail",
                message: err
            })
        })
}

function me(req, res) {
    let token = req.headers.authorization;
    let payload;

    try {
        payload = jwt.verify(token, 'secretkey');
    }catch(err){
        return res.status(422).json({
            status:"fail",
            message:"invalid token"
        })
    }

    User.findByPk(payload.id).then((data) =>{
        res.status(201).json({
            status:"success",
            data
        })
    }).catch((err) => {
        res.status(422).json({
            status:"fail",
            message:[err.message]
        })
    })
}
module.exports = {
    register,
    login,
    me
}

// class User {
//     static properties = [
//         "email",
//         "password"
//     ]

//     register(req, res) {
//         User.create({
//             email: req.body.email,
//             password: req.body.password
//         })
//             .then((data) => {
//                 res.status(200).json({
//                     status: "success",
//                     message: data
//                 })
//             })
//             .catch((err) => {
//                 res.status(400).json({
//                     status:"fail",
//                     message:[err.message]
//                 })
//             })
//     }

// }