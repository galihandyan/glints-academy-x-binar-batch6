const Product = require('../models').Product;
const jwt = require('jsonwebtoken')

// Product.create({
//   name: "Coca Cola",
//   price: 5000,
//   stock: 1
// }).then(data => console.log(data));


function create(req, res) {
    let token = req.headers.authorization;
    let payload;

    try {
        payload = jwt.verify(token, 'secretkey');
    } catch (err) {
        return res.status(422).json({
            status: "fail",
            message: [err.message]
        })
    }

    Product.create({
        name: req.body.name,
        price: req.body.price,
        stock: req.body.stock
    }).then(data =>
        res.status(401).json({
            status: true,
            data
        })
    )
}

function read(req, res) {
    Product.findAll().then(data =>
        res.status(201).json({
            status: "create",
            data
        })
    )
}


function readOne(req, res) {
    Product.findByPk(req.params.productId).then(data =>
        res.status(201).json({
            status: "create",
            data
        })
    )
}

function update(req, res) {
    let token = req.headers.authorization;
    let payload;

    try {
        payload = jwt.verify(token, 'secretkey');
    } catch (err) {
        return res.status(422).json({
            status: "fail",
            message: [err.message]
        })
    }

    Product.update({
        name: req.body.name,
        price: req.body.price,
        stock: req.body.stock
    }, {
        where: { id: req.params.productId }
    }).then(data =>
        res.status(201).json({
            status: "updated",
            data
        })
    )

}

function del(req, res) {
    let token = req.headers.authorization;
    let payload;

    try {
        payload = jwt.verify(token, 'secretkey');
    } catch (err) {
        return res.status(422).json({
            status: "fail",
            message: [err.message]
        })
    }

    Product.destroy({
        where: {
            id: req.params.productId
        }
    }).then(data =>
        res.status(200).json({
            status: "success",
            data
        }))
}

module.exports = {
    create,
    read,
    readOne,
    update,
    del
}