const express = require('express');
const router = express.Router();
const product = require('./controller/productController');
const user = require('./controller/userController');


/* Product API Collection */
router.get('/products', product.read);
router.post('/products/add', product.create);
router.put(`/products/update/:productId`, product.update);
router.delete('/products/delete/:productId', product.del);
router.get('/products/:productId', product.readOne);


/* User API Collection */
router.post('/user/registration', user.register);
router.post('/user/login', user.login);
router.get('/user/me', user.me);

module.exports = router;