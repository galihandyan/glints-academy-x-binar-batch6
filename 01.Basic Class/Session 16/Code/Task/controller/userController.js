const User = require('../models').Users;

const bcrypt = require('bcryptjs');

function register(req, res) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.password, salt);

    User.create({
        email: req.body.email.toLowerCase(),
        password: hash
    }).then(data =>
        res.status(401).json({
            status: "success",
            data
        })
    );
}

function login(req, res) {

    User.findOne({
        where: {
            email: req.body.email.toLowerCase()
        }
    })
    .then((data) => {
        if(bcrypt.compareSync(req.body.password, data.password)){
            res.status(201).json({
                status:"success",
                message:"login success"
            })
        }else{
            res.status(400).json({
                status:"fail",
                message:"Incorret Password"
            })
        }
    })
}
module.exports = {
    register,
    login
}