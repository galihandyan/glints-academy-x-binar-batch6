const fs = require('fs');

class Record {
    constructor(props) {
        if (this.constructor == Record)
            throw new Error("Can't instantiate from Record");

        this._validate(props);
        this._set(props);
    }

    _validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
            throw new Error("Props must be an object");


        this.constructor.properties.forEach(i => {
            if (!Object.keys(props).includes(i))
                throw new Error(`${this.constructor.name}: ${i} is required`)
        })
    }

    _set(props) {
        this.constructor.properties.forEach(i => {
            this[i] = props[i];
        })
    }

    get all() {
        try {
            return require(`${__dirname}/${this.constructor.name}.json`)
        }
        catch {
            return []
        }
    }

    find(id) {

    }

    update(id) {

    }

    delete(id) {

    }

    save() {
        fs.writeFileSync(
            `${__dirname}/${this.constructor.name}.json`,
            JSON.stringify([...this.all, { id: this.all.length + 1, ...this }], null, 2)
        );
    }
}


class User extends Record {

    static properties = [
        "email",
        "password"
    ]

    constructor(props) {
        super(props);
    }

}

/*
  Make two class who inherit Abstract Class called Record 

  Book,
    title
    author
    price
    publisher

  Product,
    name,
    price,
    stock
*/