class Human {
    constructor(props) {
        if (this.constructor === Human) {
            throw new Error("Can't instantiate from Human");
        }

        this.name = props.name;
        this.gender = props.gender;
    }


    introduction() {
        console.log(`Hi, my name is ${this.name}`);
    }

    work() {

    }

    marry() {

    }
}

class Police extends Human {
    constructor(props) {
        super(props);
    }

    introduction() {
        super.introduction();
        console.log(`and im a police`);
    }
}

const police1 = new Police({
    name:"Budi",
    gender:"Male"
});

police1.introduction();