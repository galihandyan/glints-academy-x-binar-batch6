const Record = require('./record.js');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

class Book extends Record {
    static properties = [
        "title",
        "author",
        "price",
        "publisher"
    ]

}

class Product extends Record {
    static properties = [
        "name",
        "price",
        "stock"
    ]
}

function start() {
    console.log(`Welcome! What do you want to do ?
    1. Input
    2. Find
    3. Update
    4. Delete `);

    rl.question("Answer: ", (answer) => {
        switch (+answer) {
            case 1:
                input();
                break;
            case 2:
                find();
                break;
            case 3:
                update();
                break;
            case 4:
                del();
                break;

            default:
                console.log("Option is not available");
                rl.close();
        }
    })
}

function input() {
    console.log(`What do you want to input ?
    1. Book
    2. Product`);

    rl.question("Answer: ", (answer) => {
        switch (+answer) {
            case 1:
                return book();
            case 2:
                return product();

            default:
                console.log("Option is not available");
        }
    })

    function book() {
        console.log("Please fill this form : ");
        rl.question("Title: ", (tit) => {
            rl.question("Author: ", (auth) => {
                rl.question("Price: ", (pri) => {
                    rl.question("Publisher: ", (pub) => {
                        let book = new Book({
                            title: tit,
                            author: auth,
                            price: pri,
                            publisher: pub
                        })

                        book.save();
                        rl.close();
                    })
                })
            })
        })
    }

    function product() {
        console.log("Please fill this form : ");
        rl.question("Name: ", (nam) => {
            rl.question("Price: ", (pri) => {
                rl.question("Stock: ", (sto) => {
                    let product = new Product({
                        name: nam,
                        price: pri,
                        stock: sto
                    })

                    product.save();
                    rl.close();
                })
            })
        })
    }
}




start();
// let Book1 = new Book({
//     tittle: "Title A",
//     author: "Author A",
//     price: "100000",
//     publisher: "Publisher A"
// })


// let Product1 = new Product({
//     name: "Procuct A",
//     price: "100000",
//     stock: "15"
// })


// Book1.save();
// Product1.save();
