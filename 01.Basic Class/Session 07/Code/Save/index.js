const readline = require('readline');
const fs = require('fs');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
})

let username;

rl.question("what is your name? ", name => {
	username = name;

	rl.close();
})

rl.on('close', () =>{
	fs.writeFileSync(
		__dirname + '/./data.json',
		JSON.stringify({
			name: username
		})
	)
})
