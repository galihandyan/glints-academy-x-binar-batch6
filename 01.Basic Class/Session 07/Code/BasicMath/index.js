const readline = require("readline");
const square = require("./square.js");

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

function getAreaInput(){
	rl.question("Side: ", answer =>{
		console.log(
			"here's the result",
			square.area(+answer)
		)
		
		rl.close()
	})
}

function getRoundInput(){
	rl.question("Side: ", answer =>{
		console.log(
			"here's the result",
			square.round(+answer)
		)

		rl.close();
	})
}

console.clear();
console.log('which operation do you want to do ?')
console.log('1. Calculate Square Area')
console.log('2. Calculate Square Round')

function handleAnswer(answer){
	console.clear();
	switch(+answer){
		case 1: return getAreaInput();
		case 2: return getRoundInput();
		default:
			console.log("Option is not available");
			rl.close();
	}
}

rl.question("Answer: ", answer =>{
	handleAnswer(answer);
});

rl.on("close", () =>{
	process.exit;
})
