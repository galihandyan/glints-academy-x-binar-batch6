let participant = [
	"Galih",
	"Fikri",
	"Nat",
	"Adrian"
];

//Conventionally, For Each
for (let i = 0; i < participant.length; i++){
	// console.log(participant[i]);
}

// built-in javascript method to do that!
participant.forEach(function(i) {
	console.log(i);
})