let participant = [
	"Galih",
	"Fikri",
	"Nat",
	"Adrian"
];

// push and pop
participant.push("hantu terakhir");
participant.pop();

// shift and unshift
participant.unshift("hantu pertama");
participant.shift();

// splice
let midIndex = Math.floor(participant.length / 2);
participant.splice(midIndex, -1, "Hantu Tengah");
participant.splice(participant.indexOf("Hantu Tengah"), 1);