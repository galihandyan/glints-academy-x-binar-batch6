const obj = {
	id: 1,
	name: "Galih",
	address: "JOgja"
}

const person = obj; //person => owned by obj

/* change person property
 * if person property change obj property will be change
 * bcs there is not copying but just rename
*/
person.name = "another person";
console.log(obj);

/*how to clone
 * using spread obj
 * this copying obj into clone
 * so you can change property without disturb the old one
*/	

const clone = {...obj} //using spread operator
clone.name = "Andyan";

console.log(clone);
console.log(obj);


//clone adn modify directly
const another = {...obj, name: "Galih", isMarried: false}
console.log('Another:', another);