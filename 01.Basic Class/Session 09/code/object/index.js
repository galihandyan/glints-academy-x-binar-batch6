// simple object

const person ={
	name: "galih",
	address: "jogja"
}


// tambah props object ada dua cara
person.isMarried = true; //Using .propsname
person.gender="Female";

person["pets"] = ["Cats", "Birds"] //using [propsname]


// dynaamicly create new props on an  object

person[person.gender == "Male" ? "Wife" : "husband"] = {
	name: "someone",
	gender :person.gender == "Male" ? "Female" : "Male"
}

console.log(person);