var id = 0;

function register(email, password, passwordConfirmation) {
	// just a validation
	if (password !== passwordConfirmation) {
		throw new Error("Password doesn't match!")
	}

	return {
		/* Autoname javascript
		 * key property name will be named same with the argumen if you doesnt declare it
		*/ 
		id: ++id,
		email,
		password
	}
}

const galih = register("andyangalih@gmail.com", "1234", "1234");
console.log(galih);