let person ={
	name: "Galih ANdyan Anindita",
	address : "Jogja"
}

//the old way
//let name =  person.name;
//let address = person.address;

//declare object destruction
let {name: fullname, address} = person;

console.log(fullname); //variabel name di rename jadi fullname
console.log(address); //nama variabel sama dengan di object person