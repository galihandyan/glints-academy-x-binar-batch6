let person = {
	name: "Galih",

	//Nested object
	address: {
		st: "Jl. Lodan"
		district: "Ngaglik"
		city: "Sleman"
		// more Nested
		provinsi: {
			id: 1,
			name: "Jawa Tengah"
		}
	}

}

// nested object
console.log(person.address.st); //jl lodan
console.log(person.address.provinsi.id); //1