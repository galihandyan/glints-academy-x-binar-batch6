const {
    Posts
} = require('../models');
const resp = require('../helper/response');

class Post {
    static async create(req, res, next) {
        try {
            let data = await Posts.create({
                title: req.body.title,
                body: req.body.body,
                users_id: req.user.id
            });

            let key = "post";
            resp(res, 201, key, data)
        } catch (err) {
            res.status(400).json({
                status: "fail",
                data: err.message
            })
        }
    }

    static async show(req, res, next) {
        try {
            let data;

            if (req.user.role == 'admin') data = await Posts.findAll();
            if (req.user.role == 'member') data = await Posts.findAll({
                where: {
                    approved: true
                }
            })
            let key = "posts"
            resp(res, 201, key, data)
        } catch (err) {
            res.status(500).json({
                status: "fail",
                data: "internal server error"
            })
        }
    }

    static async approve(req, res, next) {
        try {
            if (req.user.role !== 'admin') throw "Do not have permission";

            let data = await Posts.update({
                approved: true
            }, {
                where: {
                    id: req.params.postId
                }
            })

            let key = "post"
            resp(res, 201, key, data)
        } catch (err) {
            res.status(403).json({
                status: "fail",
                data: err
            })
        }
    }

    static async update(req, res, next) {
        try {
            if (req.user.role === 'admin') throw "You can't edit this post!";

            let data = await Posts.findByPk(req.params.postId);
            if (req.user.id !== data.users_id) throw "You do not have permission to edit this file"

            await Posts.update(req.body, {
                where: {
                    id: req.params.postId
                }
            })

            let newData = await Posts.findByPk(req.params.postId);
            let key = "New Data"
            resp(res, 201, key, newData);

        } catch (err) {
            res.status(400).json({
                status: "fail",
                data: err.message
            })
        }
    }

    static async del(req, res, next) {
        try {
            if (req.user.role !== 'member') throw "You can't delete post!";

            let data = await Posts.findByPk(req.params.postId);

            if (req.user.id !== data.users_id) throw "You do not have permission to delete this file";

            await Posts.destroy({
                where: {
                    id: req.params.postId
                }
            });

            let key = "Old Data"
            resp(res, 201, key, data);

        } catch (err) {
            res.status(400).json({
                status:"fail",
                data: err
            })
        }
    }
}

module.exports = Post;