const express = require('express');
const router = express.Router();
const User = require('../controller/user-controller');
const Post = require('../controller/post-controller');
const auth = require('../middleware/authenticate');

// router for user controller
router.post('/user/register', User.register);
router.post('/user/login', User.login);

// router for post controller
router.post('/post/add', auth, Post.create);
router.get('/post/', auth, Post.show);
router.put('/post/approve/:postId', auth, Post.approve);
router.put('/post/:postId', auth, Post.update);
router.delete('/post/:postId', auth, Post.del);

module.exports = router;