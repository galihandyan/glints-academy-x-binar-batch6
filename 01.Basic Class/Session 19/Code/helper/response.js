module.exports = (res, code, key, data) => {
    res.status(code).json({
        status: "success",
        data: {
            [key]: data
        }
    })
}