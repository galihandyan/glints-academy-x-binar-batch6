const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */


let temp; // Create temp variable for the buble sort method

// Optional
function clean(data) {
	return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
	// Code Here

	/* Check first index array value greater than second index array value 
	 * if true swap both of them
	 * how to swap =>
	 * -- put the first index value to temp
	 * -- put the second index value to first index
	 * -- and fill the second index with temp value
	 * -- change swap condition to true for indicate array value has been swap
	 * if false go to the next index and repeat to compare with the next index untill the end of array
	 * repeat from the first step untill all array sorted
	 */
	let temp; // Create temp variable for the buble sort method

	const newData = clean(data); // Clean data from null then put the value to the variable newData
	let length = newData.length; // variable for safe value of newData.length
	let swap; // variable to indicate array value has been swap

	do {
		swap = false;
		for (let i = 0; i < length; i++) {
			if (newData[i] > newData[i + 1]) {
				temp = newData[i];
				newData[i] = newData[i + 1];
				newData[i + 1] = temp;
				swap = true
			}
		}
	} while (swap);


	return newData;
}

// Should return array
function sortDecending(data) {
	// Code Here

	/* Check first index array value smaller than second index array value 
	 * if true swap both of them
	 * how to swap =>
	 * -- put the first index value to temp
	 * -- put the second index value to first index
	 * -- and fill the second index with temp value
	 * -- change swap condition to true for indicate array value has been swap
	 * if false go to the next index and repeat to compare with the next index untill the end of array
	 * repeat from the first step untill all array sorted
	 */

	let temp; // Create temp variable for the buble sort method
	const newData = clean(data); // Clean data from null then put the value to the variable newData
	let length = newData.length; // variable for safe value of newData.length
	let swap; // variable to indicate array value has been swap

	do {
		swap = false;
		for (let i = 0; i < length; i++) {
			if (newData[i] < newData[i + 1]) {
				temp = newData[i];
				newData[i] = newData[i + 1];
				newData[i + 1] = temp;
				swap = true
			}
		}
	} while (swap);


	return newData;
}


// DON'T CHANGE
test(sortAscending, sortDecending, data);
