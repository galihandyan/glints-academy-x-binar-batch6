const express = require('express');
const app = express();
const router = require('./router');
const cors = require('cors');

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.json({
        status:true,
        message:"Welcome to Daily Task Session 15"
    })
})
app.use('/', router);

app.listen(3000, () =>{
    console.log(`Server running on PORT 3000
    Time: ${Date.now()}`);
})

