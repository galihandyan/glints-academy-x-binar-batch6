const products = require('./products.json');
const users = require('./user.json');

/*
  Code Challenge #3

  Your goals to create these endpoint
    
    /products
      This will show all products on products.json as JSON Response

    /products/available
      This will show the products which its stock more than 0 as JSON Response

    /users
      This will show the users data inside the users.json,
      But don't show the password!

  */

const http = require('http');

/* Code Here */
// users.password = "*****";

// console.log(users);



const PORT = 5000;
const app = http.createServer((req, res) => {
  switch (req.url) {
    case '/products':
      res.write(JSON.stringify(products));
      break;

    case '/products/available':
      res.write(JSON.stringify(
        products.filter(function (product) {
          return product.stock != 0;
        })
      ))
      break;

    case '/users':
      delete(users.password);
      res.write(JSON.stringify(users))
      break;

    default:
      res.writeHead(404);
      res.write("404 page not found\n")
      break;
  }

  res.end();
})
app.listen(PORT, () => console.log(`Listen to PORT ${PORT}`));