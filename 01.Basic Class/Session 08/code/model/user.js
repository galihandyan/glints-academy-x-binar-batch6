const fs = require('fs');
const path = require('path');
const readline = require('../lib/readline.js')

function register(email, pwd){
	const dbPath = path.resolve(__dirname, '..', 'db', 'user.json');
	const db = require('../db/user.json');
	const user = {
		email: email,
		password: pwd
	}
	for( i = 0; i < db.length; i++){
		if (db[i].email == email){
			console.log("Email Already Exist, Use another email");
			readline.close();
		}else{
			db.push(user);
			fs.writeFileSync(dbPath, JSON.stringify(db, null,2 ));
			readline.close();
			process.exit();
		}
	}
}

function login(email, pwd){
	const db = require('../db/user.json');
	for(let i = 0; i < db.length; i++){
		if (db[i].email == email){
			if (db[i].password === pwd){
				console.log("Login Success");
				readline.close();
			}
		}else{
			console.log("Email or Password is not valid");
			readline.close();
		}
	}
}

module.exports = {
	register,
	login
}
