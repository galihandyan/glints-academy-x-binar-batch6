// pseudocode
// -----------------------------------------
// 1. what the user want to do
// 2. answer == login 
	// 2.1. input login form
	// 2.2. check db
		// 2.2.1. input == db
			// 2.2.1.1 success message
		// 2.2.2. input != db
			// 2.2.2.1 error message
// 3. answer == register
	// 3.1. input register form
	// 3.2. check db
		// 3.2.1  input != db
			// 3.2.1.1 push input to db
		// 3.2.2 input == db
			// 3.2.2.1 user already exist
// 4. answer is not available 
// -----------------------------------------

const readline = require('./lib/readline.js');
const user = require('./model/user.js');

function askForRegister() {
	console.clear();
	console.log("Glints Registration Form");
	readline.question("Email: ", email => {
		readline.question("Password: ", pwd => {
			user.register(email, pwd);
		})
	})
}


function askForLogin(){
	console.clear();
	console.log("Glints Login Form");
	readline.question("Email: ", email =>{
		readline.question("Password: ", pwd =>{
			user.login(email, pwd);
		})
	})
}

function firstRun(){
	console.log("What do you want to do ?");
	console.log("1. Login");
	console.log("2. Register");
	readline.question("Answer: ", answer=> {
		switch (+answer){
			case 1: return askForLogin();
			case 2: return askForRegister();
			default: console.log("Option is Not Available");
				readline.close()
		}
	})
}

firstRun();