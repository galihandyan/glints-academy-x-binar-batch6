const express = require('express');
const router = express.Router();
const controlIndex = require('./controller/index');
const controlPost = require('./controller/post');

router.get('/', controlIndex.home);
router.get('/findpost', controlPost.index);
router.post('/createpost', controlPost.create);
router.put('/post/:postId', controlPost.update);
router.delete('/post/:postId', controlPost.delete);

module.exports = router;