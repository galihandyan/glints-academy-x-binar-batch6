const {
    Post
} = require('../models');

module.exports = {
    index(req, res) {
        Post.findAll()
            .then(posts => {
                res.status(200).json({
                    status: 'success',
                    data: {
                        posts
                    }
                })
            })
    },

    create(req, res) {
        Post.create(req.body)
            .then(posts => {
                res.status(201).json({
                    status: 'success',
                    data: {
                        posts
                    }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    message: {
                        err
                    }
                })
            })
    },

    update(req, res) {
        Post.update(req.body, {
                where: {
                    id: req.params.postId
                }
            })
            .then(posts => {
                res.status(202).json({
                    status: 'success',
                    message: {
                        posts
                    }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    message: {
                        err
                    }
                })
            })
    },

    delete(req, res) {
        Post.destroy({
                where: {
                    id: req.params.postId
                }
            })
            .then(posts => {
                res.status(200).json({
                    status: 'success',
                    message: posts
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    message: err
                })
            })
    }
}