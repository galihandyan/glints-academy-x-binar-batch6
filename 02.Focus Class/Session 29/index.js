const express = require('express');
const app = express();
const morgan = require('morgan');
const router = require('./router')

app.set('view engine', 'pug');
app.use(express.json());
app.use(morgan('tiny'));

app.use('/', router);

app.listen(3000, () => {
    console.log("Server Running");
})