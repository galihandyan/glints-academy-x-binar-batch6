const Post = require('./models');

module.exports = {
    create: async (req, res) => {
        try {
            const data = Post.create(req.body);

            res.status(200).json({
                status: 'success',
                data: data
            })
        } catch (err) {
            res.status(400).json({
                status: 'fail',
                data: [err.message]
            })
        }
    },

    read: async (req, res) => {
        try {
            const data = Post.findAll();

            res.status(200).json({
                status: 'success',
                data: data
            })
        } catch (err) {
            res.status(400).json({
                status: 'fail',
                data: [err.message]
            })
        }
    },

    update: async (req, res) => {
        try {
            const data = Post.update(req.body, {
                where: {
                    id: req.params.id
                }
            })

            res.status(200).json({
                status: 'success',
                data: data
            })
        } catch (err) {
            res.status(400).json({
                status: 'fail',
                data: [err.message]
            })
        }
    },

    delete: async (req, res) => {
        try {
            const data = Post.delete({
                where: {
                    id: req.params.id
                }
            })

            res.status(200).json({
                status: 'success',
                data: data
            })
        } catch (error) {
            res.status(400).json({
                status: 'fail',
                data: [err.message]
            })
        }
    },

    home: (req, res) => {
        res.render('index', {
            title: 'ini title',
            message: 'ini message'
        });
    }
}