const router = require('express').Router();
const cont = require('./controller');

router.post('/api/content', cont.create);
router.get('/api/content', cont.read);
router.put('/api/content/:id', cont.update);
router.delete('/api/content/:id', cont.delete);
router.get('/home', cont.home)

module.exports = router;