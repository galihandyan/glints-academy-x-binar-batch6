const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    title: {
        type: 'string',
        required: true
    },
    body: {
        type: 'string',
        required: true
    },
    approved: {
        type: 'boolean',
        required: true,
        default: false
    }
});
const Post = mongoose.model('Post', schema);

module.exports = Post;