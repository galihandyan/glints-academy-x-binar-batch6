const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    email: {
        type: 'string',
        required: true
    },
    password: {
        type: 'string',
        required: true
    }
});
const User = mongoose.model('User', schema);

module.exports = User;