const router = require('express').Router();

// Controllers
const index = require('./controllers/indexController')

router.get('/', index.home)

module.exports = router;