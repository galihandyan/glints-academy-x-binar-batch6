const Post = require('../models/post');

module.exports = {
    all(req, res) {
        Post.find()
            .then((result) => {
                res.status(200).json({
                    status: 'success',
                    data: {
                        result
                    }
                })
            })
    },

    create(req, res) {
        const {
            title,
            body
        } = req.body;
        Post.create({
                title,
                body
            })
            .then((result) => {
                res.status(201).json({
                    status: 'success',
                    data: {
                        result
                    }
                })
            }).catch((err) => {
                res.status(422).json({
                    status: 'fail',
                    data: [err.message]
                })
            });
    }
}