const imagekit = require('../lib/imagekit')

module.exports ={
    home(req,res){
        res.status(200).json({
            status:'Success',
            message:'Hello World'
        })
    },

    uploadToCDN(){
        console.log(req.file);
        res.end();

        imagekit.upload({
            file : req.file.buffer, //required
            fileName : req.file.originalName,   //required
        }).then(response => {
            console.log(response);
        }).catch(error => {
            console.log(error);
        });
    }
}