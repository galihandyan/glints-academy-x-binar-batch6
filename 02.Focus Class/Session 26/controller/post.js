const {
    Post
} = require('../models');
const imagekit = require('../lib/imagekit');

// dadada
module.exports = {
    index(req, res) {
        Post.findAll()
            .then(posts => {
                res.status(200).json({
                    status: 'success',
                    data: {
                        posts
                    }
                })
            })
    },

    create: async (req, res) => {
        try {
            image = await imagekit.upload({
                file: req.file.buffer, //required
                fileName: req.file.originalname, //required
            })

            data = await Post.create({
                title: req.body.title,
                body: req.body.body,
                image: image.url
            })

            res.status(201).json({
                status: 'success',
                data: {
                    data
                }
            })
        } catch (err) {
            res.status(422).json({
                status: 'fail',
                message: err.message
            })
        }
    },

    update(req, res) {
        Post.update(req.body, {
                where: {
                    id: req.params.postId
                }
            })
            .then(posts => {
                res.status(202).json({
                    status: 'success',
                    message: {
                        posts
                    }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    message: {
                        err
                    }
                })
            })
    },

    delete(req, res) {
        Post.destroy({
                where: {
                    id: req.params.postId
                }
            })
            .then(posts => {
                res.status(200).json({
                    status: 'success',
                    message: posts
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    message: err
                })
            })
    }
}