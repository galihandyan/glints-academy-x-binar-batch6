const express = require('express');
const router = express.Router();
const controlIndex = require('./controller/index');
const controlPost = require('./controller/post');
const upload = require('./middleware/uploader')
const multer = require('./controller/testMulter')

router.get('/', controlIndex.home);
router.get('/findpost', controlPost.index);
router.post('/createpost',upload.single('image'), controlPost.create);
router.put('/post/:postId', controlPost.update);
router.delete('/post/:postId', controlPost.delete);
router.post('/cdn', upload.single('image'), controlIndex.uploadToCDN);

module.exports = router;