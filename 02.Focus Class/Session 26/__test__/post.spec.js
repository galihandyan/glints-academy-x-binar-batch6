const request = require('supertest');
const app = require('../index');
const {
    Post
} = require('../models');

describe('Post API Collection', () => {
    beforeAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    afterAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    describe('POST /createpost', () => {
        test('Should succesfully create new post', done => {
            request(app)
                .post('/createpost')
                .set('Content-Type', 'application/json')
                .send({
                    title: 'Hello World',
                    body: 'Lorem Ipsum'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should unsuccesfully create new post', done => {
            request(app)
                .post('/createpost')
                .set('Content-type', 'application/json')
                .send({
                    title: 'Hello Wold',
                    body: null
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('GET /findpost', () => {
        test('Should succesfully get all post', done => {
            request(app).get('/findpost')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('PUT /post/:idPost', () => {
        test('Should succesfully update post', done => {
            request(app)
                .put('/post/1')
                .set('Content-type', 'application/json')
                .send({
                    title: 'Hello World',
                    body: 'Lorem Ipsum'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(202);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should unsuccesfully update post', done => {
            request(app)
                .put('/post/1')
                .set('Content-type', 'application/json')
                .send({
                    title: '',
                    body: null
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('DELETE /post/:idPost', () => {
        test('Should succesfully delete post', done => {
            request(app)
                .delete('/post/1')
                .set('Content-type', 'application/json')
                .then(res=>{
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should unsuccesfully delete post', done => {
            request(app)
                .delete(`/post/'1'`)
                .set('Content-type', 'application/json')
                .then(res=>{
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})