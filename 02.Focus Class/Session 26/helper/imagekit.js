const imagekit = require('imagekit');

module.exports = imagekit.upload({
    file : req.file.buffer, //required
    fileName : req.file.originalName,   //required
}).then(response => {
    console.log(response);
}).catch(error => {
    console.log(error);
});