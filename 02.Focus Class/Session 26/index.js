require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');
const router = require('./router');

app.use(express.json());
if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'))
app.use(router);

module.exports = app;