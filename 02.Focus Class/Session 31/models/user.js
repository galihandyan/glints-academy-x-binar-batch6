'use strict'

const bcrypt = require('bcrypt')
// const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: {
          args: true,
          msg: 'Invalid Email'
        }
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    underscored: true,
  })

  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
      }
    }
  })

  // "Private" static _encrypt
  User._encrypt = password => bcrypt.hashSync(password, 10)

  // Public static register
  User.register = async function({ email, password }) {
    return this.create({ email, encrypted_password: this._encrypt(password) })
  }

  User.prototype.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.encrypted_password)
  }

  User.authenticate = async function({ email, password }) {
    const instance = await this.findOne({
      where: { email }
    })

    if (!instance) return Promise.reject(new Error('Email hasn\'t registered'))
    if (!instance.verifyPassword(password))
      return Promise.reject(new Error('Wrong password'))

    return Promise.resolve(instance)
  }

  return User
}
