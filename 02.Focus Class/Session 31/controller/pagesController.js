const {
  User
} = require('../models')


module.exports = {
  auth: {
    login: (req, res) => {
      res.render('pages/login')
    },
    me: (req, res) => {
    //   console.log('Is Authenticated ? ', req.isAuthenticated())
      res.render('pages/me', {
        user: req.user
      })
    },
    logout: (req,res) => {
        req.logout()
        res.redirect('/pages/login')
    }
  },

  register: (req, res) => {
    User.register(req.body)
      .then(user => {
        res.status(201).json({
          message: user
        })
      })
      .catch(err => {
        res.status(422).json({
          message: err.message
        })
      })
  }
}