require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const app = express()
const session = require('express-session')
const flash = require('express-flash')
const passport = require('./lib/passport')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(morgan('dev'))
app.use(flash())
app.use(
  session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: false
  })
)
app.use(passport.initialize())
app.use(passport.session())
app.set('view engine', 'pug')

const router = require('./router')
app.use(router)

module.exports = app
