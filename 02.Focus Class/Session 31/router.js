const router = require('express').Router()
const pages = require('./controller/pagesController')
const passport = require('./lib/passport')
const auth = require('./middleware/auth')
router.get('/admin/login', pages.auth.login)
router.get('/admin/me',auth, pages.auth.me)

router.post('/admin/api/v1/auth/login',
  passport.authenticate('local', {
    successRedirect: '/berhasilLogin',
    failureRedirect: '/admin/login',
    failureFlash: true
  })
)
router.get('/admin/api/v1/auth/logout', pages.auth.logout)

router.post('/admin/api/v1/auth/register', pages.register)

module.exports = router