module.exports = async (req, res, next) => {
    if (req.isAuthenticated() == false) {
        res.redirect('/admin/login')
    } else {
        next()
    }
}